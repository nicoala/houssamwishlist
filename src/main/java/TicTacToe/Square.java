package TicTacToe;

public enum Square {
    EMPTY(true, " "),
    CIRCLE(false, "O"),
    CROSS(false, "X");

    boolean empty;
    String representation;

    Square(boolean empty, String representation) {
        this.empty = empty;
        this.representation = representation;
    }

    public boolean isEmpty() {
        return empty;
    }

    @Override
    public String toString() {
        return representation;
    }
}
