package TicTacToe;

class Console {

     static void displayGame(boolean isOver, Board board, Square currentPlayer) {

        System.out.print(board.prepareToDisplay());
        System.out.println();

        if (isOver) {
            System.out.println("The Game is over!");
        } else {
            System.out.println("It's the " + currentPlayer + " player's turn!");
        }
    }

    static void displayWinner(Square square) {
        System.out.println("Player " + square + " wins!");
    }

    static void displayDraw() {
        System.out.println("It's a Draw!");
    }
}
