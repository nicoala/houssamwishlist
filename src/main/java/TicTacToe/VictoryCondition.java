package TicTacToe;

import java.util.function.Function;

class VictoryCondition {

    Function<Board, Boolean> function;

    VictoryCondition(Function<Board, Boolean> f) {
        function = f;
    }

    boolean apply(Board board) {
        return function.apply(board);
    }
}
