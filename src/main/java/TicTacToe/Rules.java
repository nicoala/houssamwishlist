package TicTacToe;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

class Rules {
    private List<VictoryCondition> rules;

    Rules() {
        this.rules = setUpRules();
    }

    private List<VictoryCondition> setUpRules() {

        Function<Board, Boolean> columnFunction = board -> board.columnIsTaken();
        Function<Board, Boolean> rowFunction = board -> board.rowIsTaken();
        Function<Board, Boolean> topToBottomDiagonalFunction = board -> board.topToBottomDiagonalIsTaken();
        Function<Board, Boolean> bottomToTopDiagonalFunction = board -> board.bottomToTopDiagonalIsTaken();

        VictoryCondition rowVictoryCondition = new VictoryCondition(rowFunction);
        VictoryCondition columnVictoryCondition = new VictoryCondition(columnFunction);
        VictoryCondition topToBottomDiagonalVictoryCondition = new VictoryCondition(topToBottomDiagonalFunction);
        VictoryCondition bottomToTopDiagonalVictoryCondition = new VictoryCondition(bottomToTopDiagonalFunction);

        return Arrays.asList(rowVictoryCondition, columnVictoryCondition, topToBottomDiagonalVictoryCondition, bottomToTopDiagonalVictoryCondition);
    }

    boolean boardMatchesRules(Board board) {
        return rules.stream().anyMatch(vc -> vc.apply(board)) || board.hasNoEmptySpace();
    }
}
