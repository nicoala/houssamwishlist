package TicTacToe;

import java.util.Arrays;

public class Board {

    private Square[][] squares;

    Board(Square[][] squares) {
        this.squares = squares;
    }

    Board() {
        squares = new Square[][]{
                {Square.EMPTY, Square.EMPTY, Square.EMPTY},
                {Square.EMPTY, Square.EMPTY, Square.EMPTY},
                {Square.EMPTY, Square.EMPTY, Square.EMPTY}};
    }

    boolean setSquareTo(Square type, int xCoordinates, int yCoordinates) {
        if (squares[xCoordinates - 1][yCoordinates - 1].isEmpty() ) {
            squares[xCoordinates - 1][yCoordinates - 1] = type;
            return true;
        }
        return false;
    }

    boolean hasNoEmptySpace() {

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (squares[i][j].isEmpty()) return false;
            }
        }
        Console.displayDraw();
        return true;
    }

    boolean columnIsTaken() {
        for (int i = 0; i < 3; i ++) {
            if (squares[0][i] == squares[1][i] && squares[1][i] == squares[2][i]) {
                Console.displayWinner(squares[1][i]);
                return true;
            }
        }
        return false;
    }

    boolean rowIsTaken() {
        for (int i = 0; i < 3; i ++) {
            if (squares[i][0] == squares[i][1] && squares[i][1] == squares[i][2]) {
                Console.displayWinner(squares[i][1]);
                return true;
            }
        }
        return false;
    }

    boolean topToBottomDiagonalIsTaken() {
        if (squares[1][1] == squares[0][0] && squares[1][1] == squares[2][2]) {
            Console.displayWinner(squares[1][1]);
            return true;
        }
        return false;
    }

    boolean bottomToTopDiagonalIsTaken() {
        if (squares[1][1] == squares[2][0] && squares[1][1] == squares[0][2]) {
            Console.displayWinner(squares[1][1]);
            return true;
        }
        return false;
    }

    String prepareToDisplay() {

        String board = "";
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board += " " + squares[i][j];
            }
            board += "\n";
        }
        return board;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Board board = (Board) o;

        return Arrays.deepEquals(squares, board.squares);
    }

    @Override
    public int hashCode() {
        return Arrays.deepHashCode(squares);
    }

    @Override
    public String toString() {
        return "Board{" +
                "squares=" + Arrays.toString(squares) +
                '}';
    }
}
