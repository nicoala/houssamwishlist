package TicTacToe;

public class TicTacToe {

    private Board board;
    private Rules rules;
    private Square currentPlayer = Square.CROSS;

    TicTacToe(Board board) {
        this.board = board;
        rules = new Rules();
    }

    TicTacToe() {
        board = new Board();
        rules = new Rules();
    }

    void placeMark(int xCoordinates, int yCoordinates) {
        if (board.setSquareTo(currentPlayer, xCoordinates, yCoordinates)) {
            currentPlayer = switchPlayer();
        }
    }

    private Square switchPlayer() {
        return currentPlayer == Square.CROSS ? Square.CIRCLE : Square.CROSS;
    }

    void display() {
        Console.displayGame(isOver(), board, currentPlayer);
    }

    boolean isOver() {
        return rules.boardMatchesRules(board);
    }

    Square getCurrentPlayer() {
        return currentPlayer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TicTacToe ticTacToe = (TicTacToe) o;

        return board != null ? board.equals(ticTacToe.board) : ticTacToe.board == null;
    }

    @Override
    public int hashCode() {
        return board != null ? board.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "TicTacToe{" +
                "board=" + board +
                '}';
    }
}
