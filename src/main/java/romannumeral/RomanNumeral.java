package romannumeral;

public class RomanNumeral {

    public String convertRomanNumber(int arabic) {

        String roman = "";

        for (Roman romanNumber : Roman.values()) {

            if (arabic >= romanNumber.value) {

                roman = roman + romanNumber.roman;
                arabic = arabic - romanNumber.value;

                if (arabic > 0) {
                    roman = roman + convertRomanNumber(arabic);
                }

                return roman;
            }
        }
        return roman;
    }

}
