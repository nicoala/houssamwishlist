package fizzbuzz;

public class IllegalFizzBuzzArgumentException extends Exception{
    String message;

    public IllegalFizzBuzzArgumentException(String message) {
        this.message = message;
    }
}
