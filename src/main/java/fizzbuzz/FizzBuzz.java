package fizzbuzz;

public class FizzBuzz {

    public static final String FIZZ = "FIZZ";
    public static final String BUZZ = "BUZZ";

    public static String getResult(int number) throws IllegalFizzBuzzArgumentException {

        if(number < 0) throw new IllegalFizzBuzzArgumentException("No negative arguments allowed");

        if(isDividableBy(15, number)) return FIZZ + BUZZ;
        if(isDividableBy(5, number)) return BUZZ;
        if(isDividableBy(3, number)) return FIZZ;

        return Integer.toString(number);
    }

    private static boolean isDividableBy(int divisor, int number) {
        return number % divisor == 0;
    }

}
