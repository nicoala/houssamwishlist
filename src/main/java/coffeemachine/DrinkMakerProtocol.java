package coffeemachine;

import coffeemachine.order.Order;
import coffeemachine.report.DrinkMakerReport;
import coffeemachine.shortage.DrinkQuantityService;

public class DrinkMakerProtocol {

    private DrinkMaker drinkMaker;
    private DrinkMakerReport report;
    private DrinkQuantityService quantityService;

    DrinkMakerProtocol(DrinkMaker drinkMaker, DrinkMakerReport report, DrinkQuantityService quantityService) {
        this.drinkMaker = drinkMaker;
        this.report = report;
        this.quantityService = quantityService;
    }

    void message(String message) {
        drinkMaker.send("M:" + message);
    }

    void order(Order order) {

        String drink = order.displayDrink();
        if (quantityService.isEmpty(drink)) {
            notifyShortage(drink);
        } else {
            machinePreparesDrink(order);
        }
    }

    private void notifyShortage(String drink) {
        quantityService.notifyMissingDrink(drink);
    }

    private void machinePreparesDrink(Order order) {
        String drinkOrder = order.prepare();
        drinkMaker.send(drinkOrder);
        report.add(order);
    }
}
