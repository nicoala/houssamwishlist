package coffeemachine.shortage;

public interface EmailNotifier {
    void notifyMissingDrink(String drink);
}
