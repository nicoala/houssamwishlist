package coffeemachine.shortage;

public interface BeverageQuantityChecker {
    boolean isEmpty(String drink);
}
