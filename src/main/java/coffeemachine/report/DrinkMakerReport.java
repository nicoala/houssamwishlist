package coffeemachine.report;

import coffeemachine.order.Order;

public class DrinkMakerReport {

    private DrinkReports reports;

    public DrinkMakerReport() {
        reports = new DrinkReports();
    }

    public String getStats() {
        if (reports.anOrderHasBeenMade()) return reports.display();
        return "No drinks have been purchased with this machine.";
    }

    public void add(Order order) {
        reports.add(order);
    }

    public String getMoneyMade() {
        return reports.calculateAmount() + "€";
    }
}
