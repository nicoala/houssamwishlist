package coffeemachine.report;

import coffeemachine.order.Drink;
import coffeemachine.order.Order;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class DrinkReports {
    private Boolean orderHasBeenMade = false;
    private List<DrinkReport> reports;

    public DrinkReports() {
        this.reports = new ArrayList<>();
        setUpReports();
    }

    private void setUpReports() {
        for (Drink drink : Drink.values()) {
            reports.add(new DrinkReport(drink));
        }
    }

    public void add(Order order) {
        if (orderIsNotAMessage(order.prepare())) {
            orderHasBeenMade = true;
            acknowledge(order);
        }
    }

    private boolean orderIsNotAMessage(String order) {
        return !order.contains("M:");
    }

    private void acknowledge(Order order) {
        for (DrinkReport report : reports) {
            if (report.matches(order)) report.addDrink(order.isExtraHot());
        }
    }

    public boolean anOrderHasBeenMade() {
        return orderHasBeenMade;
    }

    public String display() {
        String formattedReport = "";
        for (DrinkReport report : reports) {
            formattedReport += report.display();
        }
        return formattedReport;
    }

    public String calculateAmount() {
        int moneyMade = 0;
        for (DrinkReport report : reports) {
            moneyMade += report.moneyMade();
        }
        return formatMoney(moneyMade);
    }

    private String formatMoney(int moneyMade) {
        NumberFormat formatter = new DecimalFormat("#0.00");
        return formatter.format((double) moneyMade/100);
    }
}
