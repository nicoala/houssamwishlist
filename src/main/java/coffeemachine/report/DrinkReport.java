package coffeemachine.report;

import coffeemachine.order.Drink;
import coffeemachine.order.Order;

class DrinkReport {

    private Drink drink;
    private DrinkStats drinkStats;

    DrinkReport(Drink drink) {
        this.drink = drink;
        drinkStats = new DrinkStats();
    }

    void addDrink(boolean isExtraHot) {
        drinkStats.addDrink(isExtraHot);
    }

    boolean matches(Order order) {
        return order.hasSameDrink(drink);
    }

    String display() {
        return drink.getName() + ": " + drinkStats.numberOfDrinks()
                + " (" + drinkStats.numberOfExtraHotDrinks() + " extra hot)\n";
    }

    int moneyMade() {
        return drinkStats.moneyMade(drink.cost());
    }
}
