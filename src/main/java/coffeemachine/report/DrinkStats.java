package coffeemachine.report;

public class DrinkStats {
    private int numberOfDrinks = 0;
    private int numberOfExtraHotDrinks = 0;

    public void addDrink(boolean isExtraHot) {
        numberOfDrinks ++;
        if (isExtraHot) numberOfExtraHotDrinks++;
    }

    public String numberOfDrinks() {
        return Integer.toString(numberOfDrinks);
    }

    public String numberOfExtraHotDrinks() {
        return Integer.toString(numberOfExtraHotDrinks);
    }

    public int moneyMade(int cost) {
        return numberOfDrinks * cost;
    }
}
