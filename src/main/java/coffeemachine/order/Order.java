package coffeemachine.order;

public class Order {

    private DrinkOrder drinkOrder;
    private int centsIntroduced;

    public Order(Drink drink, int numberOfSugars, int centsIntroduced, boolean extraHot) {
        drinkOrder = new DrinkOrder(drink, numberOfSugars, extraHot);
        this.centsIntroduced = centsIntroduced;
    }

    public boolean hasSameDrink(Drink drink) {
        return drink == drinkOrder.getDrink();
    }

    public String displayDrink() {
        return drinkOrder.getDrink().getName();
    }

    public String prepare() {
        return drinkOrder.setUp(centsIntroduced);
    }

    public boolean isExtraHot() {
        return drinkOrder.isExtraHot();
    }
}
