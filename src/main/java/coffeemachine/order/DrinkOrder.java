package coffeemachine.order;

class DrinkOrder {
    private Drink drink;
    private DrinkExtras drinkExtras;

    public DrinkOrder(Drink drink, int numberOfSugars, boolean isExtraHot) {
        this.drink = drink;
        this.drinkExtras = new DrinkExtras(numberOfSugars, isExtraHot);
    }

    public String setUp(int centsIntroduced) {
        if (drink.moneyIsEnough(centsIntroduced)) {
            return drink.code() + drinkExtras.withExtraHot() + ":" + drinkExtras.withSugar();
        } else {
            return moneyIsMissing(centsIntroduced);
        }
    }

    private String moneyIsMissing(int centsIntroduced) {
        return "M:missing 0." + drink.missingMoney(centsIntroduced) +"€";
    }

    public boolean isExtraHot() {
        return drinkExtras.isHot();
    }

    public Drink getDrink() {
        return drink;
    }
}
