package coffeemachine.order;

public enum Drink {
    COFFEE("Coffee", "C", 60),
    TEA("Tea", "T", 40),
    CHOCOLATE("Chocolate","H", 50),
    ORANGE_JUICE("Orange Juice", "O", 60);

    private String name;
    private String code;
    private int cents;

    Drink(String name, String code, int cents) {
        this.name = name;
        this.code = code;
        this.cents = cents;
    }

    public String getName() {
        return name;
    }

    public String code() {
        return code;
    }

    public boolean moneyIsEnough(int moneyIntroduced) {
        return moneyIntroduced >= cents;
    }

    public int missingMoney(int moneyIntroduced) {
        return cents - moneyIntroduced;
    }

    public int cost() {
        return cents;
    }
}
