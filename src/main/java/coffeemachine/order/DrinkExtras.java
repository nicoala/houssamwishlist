package coffeemachine.order;

public class DrinkExtras {

    private final int numberOfSugars;
    private final boolean isExtraHot;

    public DrinkExtras(int numberOfSugars, boolean isExtraHot) {

        this.numberOfSugars = numberOfSugars;
        this.isExtraHot = isExtraHot;
    }

    public String withSugar() {
        return numberOfSugars > 0 ? Integer.toString(numberOfSugars) + ":0" : ":";
    }

    public String withExtraHot() {
        return isExtraHot ? "h" : "";
    }

    public boolean isHot() {
        return isExtraHot;
    }
}
