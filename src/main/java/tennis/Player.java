package tennis;

public class Player {

    private String name;
    private int score = 0;

    public Player(String name) {
        this.name = name;
    }

    public void hasScored(int scoredPoints) {
        score = scoredPoints;
    }

    public String tennisScore() {
        return TennisScore.equivalentFor(score);
    }

    public boolean isAboutToWin() {
        return score > 3;
    }

    public boolean leads(Player otherPlayer) {
        return score > otherPlayer.score;
    }

    public String closeGameWith(Player otherPlayer) {
        int difference = score - otherPlayer.score;

        if (difference > 1) return ScoreFormat.win(name);

        if (difference == 1) return ScoreFormat.advantage(name);

        if (difference == -1) return ScoreFormat.advantage(otherPlayer.name);

        if (difference < -1) return ScoreFormat.win(otherPlayer.name);

        return "";

    }
}
