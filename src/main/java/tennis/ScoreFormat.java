package tennis;

public class ScoreFormat {

    public static String tieGame(String[] scores) {
        return scores[1] + "-All";
    }

    public static String tieGameAtDeuce(String score) {
        if (score.equals("Deuce-All")) {
            score = score.replace("-All", "");
        }
        return score;
    }

    public static String win(String name) {
        return name + " wins";
    }

    public static String advantage(String name) {
        return "Advantage " + name;
    }
}
