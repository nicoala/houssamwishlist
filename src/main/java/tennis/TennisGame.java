package tennis;

public class TennisGame {

    private Player playerOne;
    private Player playerTwo;

    public TennisGame() {
        playerOne = new Player("Federer");
        playerTwo = new Player("Nadal");

    }

    public String getScore() {
        String score = playerOne.tennisScore() + "-" + playerTwo.tennisScore();

        if (playerOne.leads(playerTwo) || playerTwo.leads(playerOne)) {
            score = gameIsOnTheLine(score);
        } else {
            score = ifPlayersAreTied(score);
        }

        return score;
    }

    private String gameIsOnTheLine(String score) {
        if (playerOne.isAboutToWin() || playerTwo.isAboutToWin()) {
            score = playerOne.closeGameWith(playerTwo);
        }
        return score;
    }

    private String ifPlayersAreTied(String score) {
        String[] scores = score.split("-");
        return playersTiedBeforeDeuce(score, scores);
    }

    private String playersTiedBeforeDeuce(String score, String[] scores) {
        if(scores[0].equals(scores[1])) {
            score = ScoreFormat.tieGame(scores);
            score = ScoreFormat.tieGameAtDeuce(score);
        }
        return score;
    }

    public void playersHaveScored(int[] points) {
        playerOne.hasScored(points[0]);
        playerTwo.hasScored(points[1]);
    }
}
