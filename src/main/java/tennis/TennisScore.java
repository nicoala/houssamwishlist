package tennis;

public enum TennisScore {
    LOVE(0, "Love"),
    FIFTEEN(1, "Fifteen"),
    THIRTY(2, "Thirty"),
    FORTY(3, "Forty"),
    DEUCE(4, "Deuce");

    int exchangesWon;
    String score;

    TennisScore(int exchangesWon, String score) {
        this.exchangesWon = exchangesWon;
        this.score = score;
    }

    public static String equivalentFor(int points) {
        for (TennisScore tennisScore : TennisScore.values()) {
            if (tennisScore.exchangesWon == points) return tennisScore.score;
        }
        return DEUCE.score;
    }
}
