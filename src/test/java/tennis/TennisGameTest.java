package tennis;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TennisGameTest {

    TennisGame tennisGame;

    @Before
    public void setUp() throws Exception {
        tennisGame = new TennisGame();
    }

    @Test
    public void players_have_yet_to_score_a_point() throws Exception {
        String score = tennisGame.getScore();
        assertEquals("Love-All", score);
    }

    @Test
    public void one_player_scores_a_point() throws Exception {
        createScore(1,0);
        String score = tennisGame.getScore();
        assertEquals("Fifteen-Love", score);
    }


    @Test
    public void players_tied() throws Exception {
        createScore(1,1);
        String score = tennisGame.getScore();
        assertEquals("Fifteen-All", score);
    }

    @Test
    public void one_player_leads_40_15() throws Exception {
        createScore(3,1);
        String score = tennisGame.getScore();
        assertEquals("Forty-Fifteen", score);
    }

    @Test
    public void players_first_tied_at_48() throws Exception {
        createScore(3,3);
        String score = tennisGame.getScore();
        assertEquals("Forty-All", score);
    }

    @Test
    public void players_tied_at_30() throws Exception {
        createScore(2,2);
        String score = tennisGame.getScore();
        assertEquals("Thirty-All", score);
    }

    @Test
    public void players_tied_at_deuce() throws Exception {
        createScore(6, 6);
        String score = tennisGame.getScore();
        assertEquals("Deuce", score);
    }

    @Test
    public void one_player_wins_the_game() throws Exception {
        createScore(4, 1);
        String score = tennisGame.getScore();
        assertEquals("Federer wins", score);
    }

    @Test
    public void other_player_wins_the_game() throws Exception {
        createScore(2, 4);
        String score = tennisGame.getScore();
        assertEquals("Nadal wins", score);
    }

    @Test
    public void advantage_player_one() throws Exception {
        createScore(4, 3);
        String score = tennisGame.getScore();
        assertEquals("Advantage Federer", score);
    }

    @Test
    public void advantage_player_two() throws Exception {
        createScore(14, 15);
        String score = tennisGame.getScore();
        assertEquals("Advantage Nadal", score);
    }

    @Test
    public void long_game() throws Exception {
        createScore(20, 18);
        String score = tennisGame.getScore();
        assertEquals("Federer wins", score);
    }

    private void createScore(int playerOneScore, int playerTwoScore) {
        int[] scoredPoints = {playerOneScore, playerTwoScore};
        tennisGame.playersHaveScored(scoredPoints);
    }
}