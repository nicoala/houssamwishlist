package fizzbuzz;

import org.junit.Test;

import static org.junit.Assert.*;

public class FizzBuzzTest {

    @Test
    public void should_return_number_when_is_one() throws Exception {
        assertEquals("1", FizzBuzz.getResult(1));
    }
    @Test
    public void should_return_number_when_is_two() throws Exception {
        assertEquals("2", FizzBuzz.getResult(2));
    }

    @Test
    public void should_fizz_when_number_is_3() throws Exception {
        assertEquals("FIZZ", FizzBuzz.getResult(3));
    }

    @Test
    public void should_fizz_when_number_is_6() throws Exception {
        assertEquals("FIZZ", FizzBuzz.getResult(6));
    }

    @Test
    public void should_fizz_when_number_is_dividable_by_3() throws Exception {
        assertEquals("FIZZ", FizzBuzz.getResult(9));
    }

    @Test
    public void should_buzz_when_number_is_five() throws Exception {
        assertEquals("BUZZ", FizzBuzz.getResult(5));
    }

    @Test
    public void should_buzz_when_number_is_dividable_by_5() throws Exception {
        assertEquals("BUZZ", FizzBuzz.getResult(5));
        assertEquals("BUZZ", FizzBuzz.getResult(10));
    }

    @Test
    public void should_fizzbuzz_when_number_is_15() throws Exception {
        assertEquals("FIZZBUZZ", FizzBuzz.getResult(15));
    }

    @Test
    public void should_fizzbuzz_when_number_dividable_by_15() throws Exception {
        assertEquals("FIZZBUZZ", FizzBuzz.getResult(15));
        assertEquals("FIZZBUZZ", FizzBuzz.getResult(30));
        assertEquals("FIZZBUZZ", FizzBuzz.getResult(45));
    }

    @Test(expected = IllegalFizzBuzzArgumentException.class)
    public void should_throw_illegal_fizzbuzz_exception_when_number_is_negative() throws Exception {
        assertEquals("-1", FizzBuzz.getResult(-1));
    }
}