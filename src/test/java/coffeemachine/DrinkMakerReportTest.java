package coffeemachine;

import coffeemachine.order.Order;
import coffeemachine.report.DrinkMakerReport;
import coffeemachine.shortage.DrinkQuantityService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static coffeemachine.order.Drink.*;
import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class DrinkMakerReportTest {

    @Mock
    DrinkMaker drinkMaker;

    DrinkMakerReport report;
    DrinkMakerProtocol protocol;

    @Before
    public void setUp() throws Exception {
        report = new DrinkMakerReport();
        protocol = new DrinkMakerProtocol(drinkMaker, new DrinkMakerReport(), new DrinkQuantityService());
    }

    @Test
    public void should_print_out_a_report_if_no_drinks_have_been_purchased() throws Exception {
        String result = report.getStats();
        assertEquals(result, "No drinks have been purchased with this machine.");
    }

    @Test
    public void should_print_a_report_for_one_coffee() throws Exception {
        Order order = new Order(COFFEE, 1, 60, false);
        report.add(order);
        String result = report.getStats();
        assertEquals(result, "Coffee: 1 (0 extra hot)\n" +
                                    "Tea: 0 (0 extra hot)\n" +
                                    "Chocolate: 0 (0 extra hot)\n" +
                                    "Orange Juice: 0 (0 extra hot)\n");
    }

    @Test
    public void should_print_a_report_for_one_hot_coffee_and_one_hot_tea() throws Exception {
        Order coffee = new Order(COFFEE, 1, 60, true);
        Order tea = new Order(TEA, 1, 60, true);
        report.add(coffee);
        report.add(tea);

        String result = report.getStats();
        assertEquals(result, "Coffee: 1 (1 extra hot)\n" +
                "Tea: 1 (1 extra hot)\n" +
                "Chocolate: 0 (0 extra hot)\n" +
                "Orange Juice: 0 (0 extra hot)\n");
    }

    @Test
    public void should_print_the_money_the_machine_made() throws Exception {
        Order coffee = new Order(COFFEE, 1, 60, true);
        Order chocolate = new Order(COFFEE, 0, 60, true);
        report.add(coffee);
        report.add(chocolate);

        String result = report.getMoneyMade();
        assertEquals(result, "1,20€");
    }

    @Test
    public void should_print_the_report_and_money_the_machine_made_with_lots_of_drinks() throws Exception {
        Order coffee = new Order(COFFEE, 1, 60, true);
        Order chocolate = new Order(CHOCOLATE, 0, 50, true);
        Order tea1 = new Order(TEA, 0, 40, true);
        Order tea2 = new Order(TEA, 0, 40, false);
        Order orange = new Order(ORANGE_JUICE, 0, 60, false);
        report.add(coffee);
        report.add(coffee);
        report.add(coffee);
        report.add(coffee);
        report.add(chocolate);
        report.add(chocolate);
        report.add(chocolate);
        report.add(chocolate);
        report.add(tea1);
        report.add(tea2);
        report.add(orange);
        report.add(orange);
        report.add(orange);
        report.add(orange);

        String result = report.getStats();
        assertEquals(result, "Coffee: 4 (4 extra hot)\n" +
                "Tea: 2 (1 extra hot)\n" +
                "Chocolate: 4 (4 extra hot)\n" +
                "Orange Juice: 4 (0 extra hot)\n");

        String moneyResult = report.getMoneyMade();
        assertEquals(moneyResult, "7,60€");

        System.out.println(result + "\n" + moneyResult);
    }
}