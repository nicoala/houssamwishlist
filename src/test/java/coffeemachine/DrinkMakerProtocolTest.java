package coffeemachine;

import coffeemachine.order.Order;
import coffeemachine.report.DrinkMakerReport;
import coffeemachine.shortage.DrinkQuantityService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static coffeemachine.order.Drink.*;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class DrinkMakerProtocolTest {

    @Mock
    DrinkMaker drinkMaker;
    @Mock
    DrinkQuantityService drinkQuantityService;

    DrinkMakerProtocol protocol;

    @Before
    public void setUp() throws Exception {
        protocol = new DrinkMakerProtocol(drinkMaker, new DrinkMakerReport(), drinkQuantityService);
    }

    @Test
    public void should_display_message_to_users() throws Exception {
        String message = "Coffee Machine is launching";
        protocol.message(message);
        verify(drinkMaker).send("M:" + message);
    }

    @Test
    public void should_order_coffee_with_one_sugar() throws Exception {
        Order order = new Order(COFFEE, 1, 60, false);
        protocol.order(order);
        verify(drinkMaker).send("C:1:0");
    }

    @Test
    public void should_order_chocolate_without_sugar() throws Exception {
        Order order = new Order(CHOCOLATE, 0, 50, false);
        protocol.order(order);
        verify(drinkMaker).send("H::");
    }

    @Test
    public void should_order_tea_with_two_sugars() throws Exception {
        Order order = new Order(TEA, 2, 40, false);
        protocol.order(order);
        verify(drinkMaker).send("T:2:0");
    }

    @Test
    public void should_make_drink_when_correct_amount() throws Exception {
        Order order = new Order(COFFEE, 1, 60, false);
        protocol.order(order);
        verify(drinkMaker).send("C:1:0");
    }

    @Test
    public void should_send_message_with_amount_of_money_missing() throws Exception {
        Order order = new Order(COFFEE, 1, 40, false);
        protocol.order(order);
        verify(drinkMaker).send("M:missing 0.20€");
    }

    @Test
    public void should_make_drink_if_more_money_is_introduced() throws Exception {
        Order order = new Order(COFFEE, 2, 80, false);
        protocol.order(order);
        verify(drinkMaker).send("C:2:0");
    }

    @Test
    public void should_make_orange_juice_for_60_cents() throws Exception {
        Order order = new Order(ORANGE_JUICE, 0, 80, false);
        protocol.order(order);
        verify(drinkMaker).send("O::");
    }

    @Test
    public void should_make_extra_hot_coffee() throws Exception {
        Order order = new Order(COFFEE, 1, 80, true);
        protocol.order(order);
        verify(drinkMaker).send("Ch:1:0");
    }

    @Test
    public void should_make_extra_hot_tea_with_two_sugars() throws Exception {
        Order order = new Order(TEA, 2, 45, true);
        protocol.order(order);
        verify(drinkMaker).send("Th:2:0");
    }

    @Test
    public void should_notify_if_missing_drink() throws Exception {
        Order order = new Order(TEA, 2, 45, true);
        Mockito.when(drinkQuantityService.isEmpty(order.displayDrink())).thenReturn(true);
        protocol.order(order);
        verify(drinkQuantityService).notifyMissingDrink(order.displayDrink());
    }

    @Test
    public void should_not_notify_if_not_missing_drink() throws Exception {
        Order order = new Order(COFFEE, 2, 60, true);
        Mockito.when(drinkQuantityService.isEmpty(order.displayDrink())).thenReturn(false);
        protocol.order(order);
        verify(drinkMaker).send("Ch:2:0");
    }
}