package TicTacToe;

import org.junit.Test;

import static org.junit.Assert.*;

public class TicTacToeTest {

    private TicTacToe game;

    @Test
    public void should_set_up_game() throws Exception {

        game = new TicTacToe();

        Square[][] squares = {
                {Square.EMPTY, Square.EMPTY, Square.EMPTY},
                {Square.EMPTY, Square.EMPTY, Square.EMPTY},
                {Square.EMPTY, Square.EMPTY, Square.EMPTY}};

        Board board = new Board(squares);

        TicTacToe expectedTicTacToe = new TicTacToe(board);
        assertEquals(expectedTicTacToe, game);
    }

    @Test
    public void cross_is_placed_on_one_spot() throws Exception {
        game = new TicTacToe();

        game.placeMark(1, 1);

        Square[][] squares = {
                {Square.CROSS, Square.EMPTY, Square.EMPTY},
                {Square.EMPTY, Square.EMPTY, Square.EMPTY},
                {Square.EMPTY, Square.EMPTY, Square.EMPTY}};

        Board board = new Board(squares);
        TicTacToe expectedTicTacToe = new TicTacToe(board);

        assertEquals(expectedTicTacToe, game);

    }

    @Test
    public void cross_is_placed_on_circle_spot() throws Exception {
        game = new TicTacToe();

        game.placeMark(1, 1);
        game.placeMark(1, 1);

        Square[][] squares = {
                {Square.CROSS, Square.EMPTY, Square.EMPTY},
                {Square.EMPTY, Square.EMPTY, Square.EMPTY},
                {Square.EMPTY, Square.EMPTY, Square.EMPTY}};

        Board board = new Board(squares);
        TicTacToe expectedTicTacToe = new TicTacToe(board);

        assertEquals(expectedTicTacToe, game);

    }

    @Test
    public void game_is_over_when_board_is_full() throws Exception {

        Square[][] squares = {
                {Square.CIRCLE, Square.CIRCLE, Square.CROSS},
                {Square.CROSS, Square.CIRCLE, Square.CIRCLE},
                {Square.CIRCLE, Square.CROSS, Square.CROSS}};

        Board board = new Board(squares);
        game = new TicTacToe(board);
        game.display();
        assertTrue(game.isOver());
    }

    @Test
    public void game_is_not_over_when_board_is_not_full() throws Exception {

        Square[][] squares = {
                {Square.CIRCLE, Square.CIRCLE, Square.CROSS},
                {Square.EMPTY, Square.CIRCLE, Square.CIRCLE},
                {Square.CIRCLE, Square.CROSS, Square.EMPTY}};

        Board board = new Board(squares);
        game = new TicTacToe(board);

        assertFalse(game.isOver());

    }

    @Test
    public void game_is_over_when_one_player_has_a_column() throws Exception {

        Square[][] squares = {
                {Square.CIRCLE, Square.CROSS, Square.CIRCLE},
                {Square.CIRCLE, Square.EMPTY, Square.CROSS},
                {Square.CIRCLE, Square.CROSS, Square.EMPTY}};

        Board board = new Board(squares);
        game = new TicTacToe(board);

        assertTrue(game.isOver());
    }

    @Test
    public void game_is_over_when_one_player_has_a_row() throws Exception {
        Square[][] squares = {
                {Square.CIRCLE, Square.CROSS, Square.CIRCLE},
                {Square.CROSS, Square.CROSS, Square.CROSS},
                {Square.CIRCLE, Square.CIRCLE, Square.EMPTY}};

        Board board = new Board(squares);
        game = new TicTacToe(board);

        assertTrue(game.isOver());
    }

    @Test
    public void game_is_over_when_diagonal_is_full() throws Exception {

        Square[][] squares = {
                {Square.CIRCLE, Square.CROSS, Square.CROSS},
                {Square.CROSS, Square.CIRCLE, Square.CROSS},
                {Square.CIRCLE, Square.CROSS, Square.CIRCLE}};

        Board board = new Board(squares);
        game = new TicTacToe(board);

        assertTrue(game.isOver());

    }

    @Test
    public void game_is_over_other_diagonal_is_full() throws Exception {
        Square[][] squares = {
                {Square.CROSS, Square.CROSS, Square.CIRCLE},
                {Square.EMPTY, Square.CIRCLE, Square.CROSS},
                {Square.CIRCLE, Square.CROSS, Square.CIRCLE}};

        Board board = new Board(squares);
        game = new TicTacToe(board);

        assertTrue(game.isOver());
    }

    @Test
    public void game_is_over_when_players_take_the_grid() throws Exception {


        Square[][] squares = {
                {Square.CIRCLE, Square.CROSS, Square.CIRCLE},
                {Square.CROSS, Square.CIRCLE, Square.EMPTY},
                {Square.CROSS, Square.EMPTY, Square.CROSS}};

        Board board = new Board(squares);

        game = new TicTacToe(board);
        game.display();

        game.placeMark(2, 3);
        game.display();

        assertFalse(game.isOver());

        game.placeMark(3, 2 );
        game.display();

        assertTrue(game.isOver());

    }

    @Test
    public void players_switch_turn_when_move_is_made() throws Exception {


        Square[][] squares = {
                {Square.CIRCLE, Square.CROSS, Square.CIRCLE},
                {Square.CROSS, Square.CIRCLE, Square.EMPTY},
                {Square.CROSS, Square.EMPTY, Square.CROSS}};

        Board board = new Board(squares);

        game = new TicTacToe(board);

        game.placeMark(3, 2);

        assertEquals(Square.CIRCLE, game.getCurrentPlayer());

    }

    @Test
    public void should_display_game() throws Exception {


        Square[][] squares = {
                {Square.CIRCLE, Square.CROSS, Square.CIRCLE},
                {Square.EMPTY, Square.CIRCLE, Square.EMPTY},
                {Square.CROSS, Square.EMPTY, Square.CROSS}};

        Board board = new Board(squares);

        game = new TicTacToe(board);

        String result = board.prepareToDisplay();

        String expected = " O X O\n" + "   O  \n" + " X   X\n";

        game.display();

        assertEquals(result, expected);

    }

    @Test
    public void should_display_a_winning_game() throws Exception {
        Square[][] squares = {
                {Square.CIRCLE, Square.CROSS, Square.CIRCLE},
                {Square.CROSS, Square.CIRCLE, Square.EMPTY},
                {Square.EMPTY, Square.EMPTY, Square.CROSS}};

        Board board = new Board(squares);

        game = new TicTacToe(board);
        game.display();

        game.placeMark(2, 3);
        game.display();

        assertFalse(game.isOver());

        game.placeMark(3, 1 );
        game.display();

        assertTrue(game.isOver());
    }
}