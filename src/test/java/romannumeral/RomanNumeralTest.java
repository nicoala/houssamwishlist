package romannumeral;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RomanNumeralTest {

    RomanNumeral romanNumeral;

    @Before
    public void setUp() throws Exception {
        romanNumeral = new RomanNumeral();
    }

    @Test
    public void should_convert_one_to_I() throws Exception {
        assertEquals("I", romanNumeral.convertRomanNumber(1));
    }

    @Test
    public void should_convert_two_to_II() throws Exception {
        assertEquals("II", romanNumeral.convertRomanNumber(2));
    }

    @Test
    public void should_convert_three_to_III() throws Exception {
        assertEquals("III", romanNumeral.convertRomanNumber(3));
    }

    @Test
    public void should_convert_five_to_V() throws Exception {
        assertEquals("V", romanNumeral.convertRomanNumber(5));
    }

    @Test
    public void should_convert_four_to_IV() throws Exception {
        assertEquals("IV", romanNumeral.convertRomanNumber(4));
    }

    @Test
    public void should_convert_6_to_VI() throws Exception {
        assertEquals("VI", romanNumeral.convertRomanNumber(6));
    }

    @Test
    public void should_convert_7_to_VII() throws Exception {
        assertEquals("VII", romanNumeral.convertRomanNumber(7));
    }

    @Test
    public void should_convert_10_to_X() throws Exception {
        assertEquals("X", romanNumeral.convertRomanNumber(10));
    }

    @Test
    public void should_convert_9_to_IX() throws Exception {
        assertEquals("IX", romanNumeral.convertRomanNumber(9));
    }

    @Test
    public void should_convert_11_to_XI() throws Exception {
        assertEquals("XI", romanNumeral.convertRomanNumber(11));
    }

    @Test
    public void should_convert_13_to_XIII() throws Exception {
        assertEquals("XIII", romanNumeral.convertRomanNumber(13));
    }

    @Test
    public void should_convert_15_to_XV() throws Exception {
        assertEquals("XV", romanNumeral.convertRomanNumber(15));
    }

    @Test
    public void should_convert_16_to_XVI() throws Exception {
        assertEquals("XVI", romanNumeral.convertRomanNumber(16));
    }

    @Test
    public void should_convert_30_to_XXX() throws Exception {
        assertEquals("XXX", romanNumeral.convertRomanNumber(30));
    }

    @Test
    public void should_convert_50_to_L() throws Exception {
        assertEquals("L", romanNumeral.convertRomanNumber(50));
    }

    @Test
    public void should_convert_44_to_XLIV() throws Exception {
        assertEquals("XLIV", romanNumeral.convertRomanNumber(44));
    }

    @Test
    public void should_convert_60_to_LX() throws Exception {
        assertEquals("LX", romanNumeral.convertRomanNumber(60));
    }

    @Test
    public void should_convert_81_to_LXXXI() throws Exception {
        assertEquals("LXXXI", romanNumeral.convertRomanNumber(81));
    }

    @Test
    public void should_convert_130_to_CXXX() throws Exception {
        assertEquals("CXXX", romanNumeral.convertRomanNumber(130));
    }

    @Test
    public void should_convert_up_to_1000() throws Exception {
        assertEquals("CCC", romanNumeral.convertRomanNumber(300));
        assertEquals("DL", romanNumeral.convertRomanNumber(550));
        assertEquals("M", romanNumeral.convertRomanNumber(1000));
        assertEquals("DCCC", romanNumeral.convertRomanNumber(800));

       // for(int i = 0; i < 1000; i++) System.out.println(romanNumeral.convertRomanNumber(i));
    }
}